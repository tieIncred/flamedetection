# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Version [0.1.1] - 2022-01-04

### Modified
- `detection.py`: 
    Modified the exsiting script to utilize ResNest class from utils
- utils
    - `resnet_infer.py`:
        Script for getting inference from ResNet34 trition client, trained on updated dataset
- `CHANGELOG.md`
- `env_var`
- `README.md`

## Version [0.1.0] - 2021-04-28

### Added
- main.py
    Main program - Code structure according to wobotteam standardised use-case structure
- detection.py
    Code which implements main logic of the use case
- utils
    - __init__.py
    - color_segmentation.py
        Program to segment white color from frame
    - motion_detection.py
        Program to detect motion
    - polygon_roi_tool.py
        Program from [Bitbucket Repo](https://bitbucket.org/wobotteam/roi_tool_polygon/src/master/)
    - stream_rtsp.py
        Helper program to stream local video as RTSP
    - triton_inference.py
        Code with methods to infer on models hosted on Triton
- Dockerfile
- .dockerignore
- build_and_push.sh
    Code to build a Docker image from Dockerfile and push to Wobot Intelligence DockerHub
- run_docker.py
    Script to read required environment variables and run the main Docker container
- WoUtils latest package (v0.2.6)
- README.md
- CHANGELOG.md