__author__ = "Tausif Iqbal"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Karan Acharya", "Sadjyot Gangolli"]
__license__ = "Proprietary"
__version__ = "0.1.1"
__maintainer__ = "Tausif Iqbal"
__email__ = "tausif.iqbal@wobot.ai"
__status__ = "Development"
__module_name__ = "[Detection]"

import cv2
import numpy as np
import tritonclient.grpc as grpcclient
from utils.yolov5m_infer import YoloV5_Triton_Client
from utils.resnet_infer import ResNet_Triton_Client
from utils.classifier_infer import ClassificationClient

# Woutils imports
# from woutils.common import MotionDetector
from woutils.infer.peoplenet_infer import PeopleNet_Triton_Client
from woutils.misc.draw_utils import draw_text, crop_polygon_roi, draw_poly
from woutils.postprocess import CentroidInPoly, get_abs_polypoints, segment_color


class MainDetector:
    def __init__(
        self,
        flame_rois,
        person_roi,
        flame_model_config,
        person_model_config,
        dev_params,
        grpc_port=8091,
    ):
        """__init__

        Args:
            flame_roi (list): List of normalised polygon ROI for flame detection
            person_roi (list): List of normalised polygon ROI for customer detection
            flame_model_config (dict): Model config
            person_model_config (dict): Model config
            dev_params (dict): CONFIG parameters
            grpc_port (int, optional): Triton port to connect to. Defaults to 8001.

        Raises:
            Exception: Exception describing failure to connect to Triton
        """

        self.flame_rois = flame_rois
        self.person_roi = person_roi
        self.grpc_port = grpc_port

        # flame Classifier params
        self.flame_model_name = flame_model_config.get_model()
        self.person_model_name = person_model_config.get_model()
        self.flame_input_layer = dev_params.get("flame_input_layer", "tensor")
        self.flame_output_layer = dev_params.get("flame_output_layer", "371")
        self.flame_input_shape = dev_params.get("flame_input_shape", 128)
        self.use_color_segmentation = dev_params.get("use_color_segmentation", False)
        self.person_detector = dev_params.get("person_detector", "peoplenet")

        # List of shades of white to segment white color
        self.rgb_list = dev_params.get("rgb_list", [[255, 255, 255]])

        # Initialise Motion Detector
        # self.motion_detector = MotionDetector(
        #     history=dev_params.get("md_history", 1000),
        #     threshold=dev_params.get("md_threshold", 64),
        # )
        # self.md_contour_ratio = dev_params.get("md_contour_ratio", 0.1)

        # Initialise Triton clients

        # flame Classifier Client
        self.flame_triton_client = ClassificationClient(
            model_name=self.flame_model_name,
            grpc_host=flame_model_config.get_host(),
            grpc_port=self.grpc_port,
            input_shape=self.flame_input_shape
        )

        # PeopleNet Client
        if self.person_detector == "peoplenet":
            self.person_triton_client = PeopleNet_Triton_Client(
                min_confidence=dev_params.get("detection_confidence", 0.7),
                nms_threshold=dev_params.get("nms_threshold", 0.25),
                grpc_host=person_model_config.get_host(),
                grpc_port=self.grpc_port,
                model_version=person_model_config.get_version(),
            )

        # YOLOv5m Client
        elif self.person_detector == "yolov5m":
            self.person_triton_client = YoloV5_Triton_Client(
                model_name=self.person_model_name,
                detection_confidence=dev_params.get("detection_confidence", 0.7),
                grpc_host=person_model_config.get_host(),
                grpc_port=self.grpc_port,
                nms_thresh=dev_params.get("nms_threshold", 0.25),
            )

    def initialise_detector(self, frame):
        """initialise_detector
        Method to initialise this class using the first successful frame
        from the stream

        Args:
            frame (np.ndarray): RTSP frame
        """
        height, width = frame.shape[:2]
        person_roi_absolute = get_abs_polypoints(self.person_roi, width, height)
        self.centroid_in_poly = CentroidInPoly(person_roi_absolute)

    def _classify_flame(self, frame):
        """_classify_flame
        Method to infer on the Triton-hosted flame Classification model

        Args:
            frame (np.ndarray): Cropped frame in the desired flame detection ROI

        Returns:
            pred (str): Value indicating presence of flame
        """
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        pred = self.flame_triton_client.infer(image)
        return pred

    def detect_person(self, frame):
        """_detect_person
        Method to infer on the Triton-hosted PeopleNet model

        Args:
            frame (np.ndarray): The entire frame from the RTSP

        Returns:
            person_is_present (boolean): True/False value indicating presence of person
            inside the ROI
        """
        frame_o = frame.copy()
        person_is_present = False

        detections = []

        # Infer using the person detection triton client based on Model Specified
        if self.person_detector == "peoplenet":
            detections = self.person_triton_client.detect(frame_o)
            label = []
            for _ in detections:
                label.append("person")

        elif self.person_detector == "yolov5m":
            boxes = self.person_triton_client.infer(frame_o)
            label = []
            if boxes:
                for box in boxes:
                    _class = box[-1]
                    if _class == 0:
                        detections.append(box[:4])
                        label.append("person")

        if detections:
            # Check whether the detected people lie inside the ROI
            cus_present_list = [
                self.centroid_in_poly.detection_in_poly(detection)
                for detection in detections
            ]
            person_is_present = any(cus_present_list)

        return person_is_present, frame_o, detections, label

    def process(self, frame):

        # take the cropped frames from all the roi and classify on them
        cropped_frames=[]
        for i in range(len(self.flame_rois)):

            # Get cropped frame for the flame ROI
            cropped_frame = crop_polygon_roi(image=frame.copy(), polygon_roi=self.flame_rois[i])
            cropped_frames.append(cropped_frame)


        # Check if Color Segmentation is Turned On/Off
        # if self.use_color_segmentation:
        #     # Detect white color from cropped_frames
        #     is_white_list=[]
        #     for cf in cropped_frames:

        #         is_white, _, _ = segment_color(
        #             cf.copy(),
        #             rgb_list=self.rgb_list,
        #             take_top_half=False,
        #             contour_area_ratio=0.01,
        #         )
        #         is_white_list.append(is_white)
            
        #     roi_is_white = any(is_white_list) # if white color is observed in any flame roi.
        # else:
        roi_has_flame = True


        # If flame is classified in any of the ROI we return flame
        flame_labels = []
        # if motion_is_detected and roi_is_white:
        if roi_has_flame:
            for cf in cropped_frames:
                flame_label = self._classify_flame(cf)
                flame_labels.append(flame_label)
                
        if "high_flame" in flame_labels:
            return "high_flame"
        else:
            return "low_flame"


    '''

    def process(self, frames, ticket_label, person_threshold):
        """
        process 
        Method to process the frame recieved from main.py
        Implements main logic of this use-case

        Args:
            frames (np.ndarray): Atleast 10 latest Frames from the RTSP video
            ticket_label: label to use if ticket raised
            person_threshold: Minimum sliding window threshold for person detection on frames.
        Returns:
            ticket_flag (boolean): Flag to raise ticket
            frame (np.ndarray): Latest processed frame
        """
        ticket_flag = False
        frame = frames[-1]
        orig_frame = frame.copy()

        # Get cropped frame for the flame ROI
        cropped_frame = crop_polygon_roi(
            image=frame.copy(), 
            polygon_roi=self.flame_roi
        )

        # Draw flame & customer roi
        frame = draw_poly(frame, self.flame_roi)
        frame = draw_poly(frame, self.person_roi)

        # Detect Motion
        contours, _, _ = self.motion_detector.detect_motion(
            cropped_frame, 
            min_contour_ratio=self.md_contour_ratio
        )
        motion_is_detected = True if len(contours) > 0 else False

        # Check if Color Segmentation is Turned On/Off
        if self.use_color_segmentation:
            # Detect white color from cropped_frame
            roi_is_white, _, _ = segment_color(
                cropped_frame.copy(), 
                rgb_list=self.rgb_list,
                take_top_half=False,
                contour_area_ratio=0.01
            )
        else:
            roi_is_white = True

        # If white color is segmented & motion is detected, classify flame
        flame_is_printed = False
        if motion_is_detected and roi_is_white:
            flame_is_printed = self._classify_flame(cropped_frame)

        # Detect Person in the whole frame(old)
        # customer_is_present, frame,  detections, label = self.detect_person(frame)
        
        ################ New logic #####################
        if flame_is_printed:
            # Initialize sliding window 
            slw = []
            # Loop over frames and store person detection in slw
            for each in frames:
                customer_is_present, f,  d, l = self.detect_person(each)
                slw.append(int(customer_is_present))

            # Apply sliding window logic
            if np.mean(slw) <= person_threshold:
                ticket_flag = True
            
            print("\n Person detection sliding window when flame detected:", slw)

        # Raise Ticket if ticket_flag = True
        if ticket_flag == True:
            # get detections for the latest frame
            customer_is_present, f, detections, label = self.detect_person(orig_frame)
            
            # draw ticket_label
            frame = draw_text(
            frame=frame,
            text=ticket_label,
            txt_color=(255, 255, 255),
            bg_color=(232, 103, 54),)

        
        else:
            detections = []
            label = None

        # Raise Ticket if flame is classified & Customer is absent
        #  if flame_is_printed and not customer_is_present:

        #     frame = draw_text(
        #         frame=frame,
        #         text=ticket_label,
        #         txt_color=(255, 255, 255),
        #         bg_color=(232, 103, 54),
        #     )

        #     # Set the flag
        #     ticket_flag = True
        
        # print(f"Motion[{motion_is_detected}], White[{roi_is_white}], flame[{flame_is_printed}], Customer[{customer_is_present}]")
        return ticket_flag, frame,  detections, label
            
    '''
