import os

DEBUG = os.environ.get("DEBUG", True)
MODELS = os.environ.get("MODELS", None)
CONFIG = os.environ.get("CONFIG", None)
PAYLOAD = os.environ.get("PAYLOAD", None)

IMAGE_NAME = "wobotintel/flame-detected-person-absent-triton:test"
CONTAINER_NAME = "flame_testing"

if MODELS is None:
    MODELS = input("Enter the json for MODELS: ").strip()
if CONFIG is None:
    CONFIG = input("Enter the json for CONFIG: ").strip()
if PAYLOAD is None:
    PAYLOAD = input("Enter the json for PAYLOAD: ").strip()

print(
    f"docker run -it --rm --net host -e DEBUG='{DEBUG}' -e MODELS='{MODELS}' -e CONFIG='{CONFIG}' -e PAYLOAD='{PAYLOAD}' --name {CONTAINER_NAME} {IMAGE_NAME}"
)
os.system(
    f"sudo docker run -it --rm --net host -e DEBUG='{DEBUG}' -e MODELS='{MODELS}' -e CONFIG='{CONFIG}' -e PAYLOAD='{PAYLOAD}' --name {CONTAINER_NAME} {IMAGE_NAME}"
)
