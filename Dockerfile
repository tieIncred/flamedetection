FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y libopencv-dev
RUN apt-get install -y python3 python3-pip
RUN apt-get install -y ffmpeg awscli nano

RUN python3 -m pip install --upgrade setuptools pip
RUN python3 -m pip install --upgrade nvidia-pyindex
RUN python3 -m pip install --upgrade tritonclient[all]

WORKDIR /home

COPY ./woutils-*.whl /home
RUN python3 -m pip install woutils-*.whl --extra-index-url https://pypi.ngc.nvidia.com

COPY . /home
CMD ["python3", "main.py"]