__author__ = "Karan Acharya"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Karan Acharya"]
__license__ = "Proprietary"
__version__ = "0.1.0"
__maintainer__ = "Karan Acharya"
__email__ = "karan.acharya@wobot.ai"
__status__ = "Development"
__module_name__ = "[Main]"

import cv2
import time
import numpy as np
from datetime import datetime
import os

# WoUtils imports
from woutils.io import VideoInput, ConfigParser
from woutils.streamer import RTSPStreamer
from woutils.io.config_parser import (
    ConfigParser,
    DashboardPayloadConfig,
)
from woutils.common import DataCollector
from woutils.misc import Alerts, calc_window_size, set_fps, get_fps
from woutils.misc.draw_utils import draw_text, crop_polygon_roi, draw_poly
from woutils.postprocess import Ticketing

# Local imports - use-case specific
from detection import MainDetector
from utils.logic import get_boxes_in_poly
from utils.grader import GradingModel

parser = ConfigParser()

# Loading main params dictionaries
dev_params = parser.get_dev_params_config()
flame_model_config = parser.get_models_config(index=0)
person_detector = dev_params.get("person_detector", "peoplenet")
if person_detector == "peoplenet":
    person_model_config = parser.get_models_config(index=1)
elif person_detector == "yolov5m":
    person_model_config = parser.get_models_config(index=2)
dashboard_payload = parser.get_dashboard_payload_config()
dashboard_payload = parser.get_raw_dashboard_payload_config()
dashboard_config = DashboardPayloadConfig(dashboard_payload)
debug = parser.get_debug_config()

# Dashboard params for particular use cas
usecase = dashboard_config.get_usecase()
company = dashboard_config.get_company()
camera_name = dashboard_config.get_cam_name()
podname = dashboard_config.get_podname()
camera = dashboard_config.get_cam_id()
rtsp = dashboard_config.get_rtsp()
location = dashboard_config.get_location()
schedule = dashboard_config.get_schedule()
cooldown_duration = dashboard_config.get_cooldown()
task = dashboard_config.get_task()
assign_task = dashboard_config.get_assign_task()
ticketing_endpoint = dashboard_config.get_ticketing_endpoint()
is_ticket = dashboard_config.get_is_ticket()
ticket_label = dashboard_config.get_ticket_label()
alerting_endpoint = dashboard_config.get_alerting_endpoint()
ticket_media_storage = dashboard_config.get_ticket_media_storage()
credential_det = dashboard_config.get_storage_info()
roi_parser = dashboard_config.get_rois()
flame_rois = roi_parser.get_specific_poly_cords("detect-flame") # multiple flame roi
person_roi = roi_parser.get_specific_poly_cords("detect-person")[0] # single person roi

# Dev params - mandatory for particular use case
target_fps = dev_params.get("target_fps", 3)  # for streamer object
ticketing_trigger_threshold = dev_params.get("ticketing_trigger_threshold", 0.8)
event_duration = dev_params.get("event_duration", 2)

streamer_app = dev_params.get("streamer_app", "flame_detect")
streamer_ip = dev_params.get("streamer_ip", "164.52.204.9")
streamer_port = int(dev_params.get("streamer_port", 8554))

# Initializing and Alerts
alerts = Alerts(
    alerting_endpoint=alerting_endpoint,
    camera=camera,
    schedule=schedule,
    assign_task=assign_task,
    source_type="cv-pod",
)

alerts.debug(" * *# " * 50)
alerts.debug(flame_model_config)
alerts.debug(person_model_config.get_model())
alerts.debug(dashboard_payload)
alerts.debug(dev_params)
alerts.debug(debug)
alerts.debug(" * *# " * 50)

# Classifier based Ticketing sliding window initialization
sliding_window_classifier = []
sliding_window_detector = []
# Hardcoded for testing
flame_window_size = target_fps
fps = target_fps


# Detector based Ticketing sliding window initialization
frames = []  # sliding list of frames
detector_sw_thresh = 0.5

# Data collector
collector = DataCollector(usecase, camera_name, podname)

## RTSP Streamer (Only in DEBUG mode)
if debug:
    debug_streamer = RTSPStreamer(
        app_name=streamer_app, server_ip=streamer_ip, server_port=streamer_port
    )

if ticket_media_storage == "azure":
    bucket_name          = credential_det.get_bucket()[1]
elif ticket_media_storage == "aws":
    bucket_name          = credential_det.get_bucket()[0]

## Ticketing
ticketing = Ticketing(
    ticketing_endpoint   = ticketing_endpoint,
    ticket_media_storage = ticket_media_storage,
    task                 = task,
    usecase              = usecase,
    dirpath              = dashboard_config.get_bucket_directory(),
    bucket_name          = bucket_name,
    aws_access_key       = credential_det.get_bucket_accesskey()[0],
    azure_access_key     = credential_det.get_bucket_accesskey()[1],
    azure_account_name   = credential_det.get_bucket_account_name()[1],
    aws_secret_key       = credential_det.get_bucket_secretkey()[0],
    aws_region           = credential_det.get_bucket_region()[0],
)

## Main Detector
try:
    main_detector = MainDetector(
        flame_rois, person_roi, flame_model_config, person_model_config, dev_params
    )
except Exception as e:
    alerts.critical(f"{__module_name__} Channel Creation Failed: {e}")


# Initialize the Ticket Grading API
grading_api = GradingModel(
    access_id  = credential_det.get_bucket_accesskey()[0],
    secret_key = credential_det.get_bucket_secretkey()[0],
    region     = credential_det.get_bucket_region()[0],
)

# Default Ticket Grade
default_grade = "unknown"

# Set the Postprocessing Config
postprocess_config = {
    "grading_low_thresh": dev_params.get("grading_low_thresh", 0.4),
    "grading_high_thresh": dev_params.get("grading_high_thresh", 0.7),
    "api_det_conf": dev_params.get("api_det_conf", 0.5),
}

## VideoInput
cap = VideoInput(rtsp)
detector_is_initialised = False
test_image = False

# TODO : Remove after testing
# os.makedirs('ticket_dir')

#################################### POSTPROCESS FUNCTION ####################################
def postprocess(detections):
    """
    postprocess:
        This function applies the basic post processing to the detections
        received by the models

    Functionality:
        - Filter Only boxes whose centroids are inside the ROI
        - Detect if any person is present without uniform

    Args:
        - detections (list) : List of all the detections present in the ticketing_frame

    Returns:
        - list : List of all the filtered and post processed detections
    """

    # Filter Only Boxes whose centroids are inside the ROI
    try:
        detections = get_boxes_in_poly(
            frame=frame,
            poly_points=person_roi,
            detections=detections,
        )
    except Exception as e:
        alerts.warning(f"{__module_name__} Polygon - Box Filtering Failed. Error: {e}")

    return detections


alerts.info(f"{__module_name__} Starting Processing for RTSP")
######################################## MAIN LOOP ###################################################
while True:

    ######################################## READING FRAME ###################################################
    if test_image:
        ret, frame = True, cv2.imread("/path/to/image.jpg")
    else:
        ret, frame = cap.get_frame()

    # Handle Broken RTSP
    if not ret:
        alerts.error(
            f"{__module_name__} RTSP Broken. Retrying after sleeping for 10 seconds. RTSP: {rtsp}"
        )
        time.sleep(10)
        cap = VideoInput(rtsp)
        continue

    flame_label = 0  # initialised as frame not processed.
    detections = []
    frame_copy = frame.copy()
    # initializing frame sliding window list.
    (dim0, dim1) = frame_copy.shape[:2]
    if dim0 > 1280 and dim1 > 720:
        frame_copy = cv2.resize(frame_copy, (1280, 720), interpolation=cv2.INTER_CUBIC)

    window_size = calc_window_size(fps, event_duration)
    frames.append(frame_copy)
    if len(frames) > window_size:
        del frames[0]

    for i in range(len(flame_rois)):
        frame = draw_poly(frame, flame_rois[i])
    frame = draw_poly(frame, person_roi)

    start_time = time.time()

    ######################################## PRE-PROCESSING & INFERENCE #######################################

    # Pre-process frame if not done,
    if not detector_is_initialised:
        main_detector.initialise_detector(frame)
        detector_is_initialised = True

    try:
        flame_label = main_detector.process(frame)

    except Exception as e:
        alerts.critical(f"Triton Inference is not working - {e} sleeping 5 seconds")
        time.sleep(5)
        continue

    ############################## SLIDING WINDOW BASED TICKETING #######################################

    if flame_label == "high_flame":
        sliding_window_classifier.append(1)
    else:
        sliding_window_classifier.append(0)

    # Maintain a Constant window Length (by sliding the window to the right)
    if len(sliding_window_classifier) > flame_window_size:
        del sliding_window_classifier[0]

    # Check Detections
    if (
        len(sliding_window_classifier) >= int(flame_window_size)
        and np.mean(sliding_window_classifier) >= ticketing_trigger_threshold
    ):

        for frame_ in frames:
            person_is_present, frame_o, detections, label = main_detector.detect_person(
                frame_
            )
            collector.collect(frame_o, detections, label)
            if person_is_present:
                sliding_window_detector.append(1)
            else:
                sliding_window_detector.append(0)
                grading_api.set_grading_frame(frame_)
                grading_api.set_grading_detections(detections)

        alerts.debug(
            f"Detector Sliding Window Mean: {np.mean(sliding_window_detector)}"
        )

        if np.mean(sliding_window_detector) <= detector_sw_thresh:

            sliding_window_detector = []

            alerts.debug(
                f"{__module_name__} flame was detected in absence of person. Ticket will be raised."
            )

            # draw ticket_label
            ticketing_frame = draw_text(
                frame=frame,
                text=ticket_label,
                txt_color=(255, 255, 255),
                bg_color=(232, 103, 54),
            )

            # TODO : Remove after testing
            # now = str(time.time()).split('.')[0]
            # cv2.imwrite(f'ticket_dir/image_{now}.jpg', ticketing_frame)

            # Grade Tickets
            try:
                ticket_grade = grading_api.grade_detections(
                    postprocess_func=postprocess, postprocess_config=postprocess_config
                )
            except Exception as e:
                alerts.critical(f"{__module_name__} Error in Grading: {e}")
                ticket_grade = default_grade

            # Generate Ticket
            try:
                ticketing.generate_ticket(
                    confidence=ticket_grade,
                    company=company,
                    location=location,
                    camera=camera,
                    schedule=schedule,
                    image=ticketing_frame,
                    is_ticket=is_ticket,
                )

            except Exception as e:
                alerts.critical(f"{__module_name__} Tickets are not being generated")

            # Sleep for some time after a ticket is raised
            time.sleep(cooldown_duration)
            sliding_window_classifier = (
                []
            )  # Reset Sliding Window after Ticket is raised

    # # Set FPS Cap, and get the current FPS
    set_fps(target_fps, start_time)
    _, fps = get_fps(start_time)

    
    ######################################## DEBUG STREAM ###################################################
    # If the Debug Flag is enabled, then stream the processed frame
    if debug:
        alerts.debug(
            f"FPS: {fps} \nWindow Length: {len(sliding_window_classifier)} \n Window Size: {int(flame_window_size)}\nWindow Value: {sliding_window_classifier}\n{'='*50}"
        )
        alerts.debug(
            f"Sliding Window Mean: {np.mean(sliding_window_classifier)} | Ticketing Trigger Threshold: {ticketing_trigger_threshold}"
        )
        try:
            debug_streamer.stream(frame=frame, fps=fps)
        except Exception as e:
            alerts.debug(
                f"{__module_name__} Error in Streaming: {e} ... Reinitializing"
            )
            debug_streamer = RTSPStreamer(
                app_name=streamer_app, server_ip=streamer_ip, server_port=streamer_port
            )

    alerts.info(
        f"{__module_name__} Classifier Output : {flame_label} , Detection Output : {detections}"
    )
