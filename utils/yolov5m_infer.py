__author__ = "Karan Shetty"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Karan Shetty", "Vinay Verma"]
__license__ = "Proprietary"
__version__ = "0.1.0"
__maintainer__ = "Karan Shetty"
__email__ = "karan.shetty@wobot.ai"
__status__ = "Development"
__module_name__ = "[YoloV5 Triton Client]"

import numpy as np
import cv2
import tritonclient.grpc as grpcclient

from woutils import logging

class YoloV5_Triton_Client:

    def __init__(
        self,
        model_name = "yolov5",
        detection_confidence=0.5,
        grpc_host="localhost",
        grpc_port=8001,
        nms_thresh = 0.6,       
        force_float_input=True,
        filter_class_id = None
    ):
        self.force_float_input = force_float_input
        self.grpc_port      = grpc_port
        self.grpc_host      = grpc_host
        self.model_name     = model_name
        self.input_width    = 640
        self.input_height   = 640
        self.conf_thresh    = detection_confidence
        self.nms_thresh     = nms_thresh
        self.filter_class_id= filter_class_id

        self.input_label    = "data"
        self.output_label   = "prob"

        self.inputs  = []
        self.outputs = []

        if self.force_float_input:
            dtype = "FP32"
        else:
            dtype = "UINT8"

        self.inputs.append(grpcclient.InferInput(
                self.input_label,
                [1, 3, self.input_width, self.input_height],
                dtype))

        self.outputs.append(
            grpcclient.InferRequestedOutput(self.output_label))

        try:
            self.triton_client = grpcclient.InferenceServerClient(
                url=f"{self.grpc_host}:{self.grpc_port}")

        except Exception as e:
            logging.error(
                f"{__module_name__} Channel Creation Failed: {e}")
            raise Exception(
                f"{__module_name__} Channel Creation Failed: {e}")


    def infer(self, input_image_path):
        # Do image preprocess
        input_image, origin_h, origin_w = self.preprocess_image(
                input_image_path
        )
        
        # Initialize the data
        self.inputs[0].set_data_from_numpy(input_image)
        # Test with outputs
        results = self.triton_client.infer(
            model_name=self.model_name,
            inputs=self.inputs,
            outputs=self.outputs,
            headers={'test': '1'}
        )
        
        # Get the output arrays from the results
        output= results.as_numpy(self.output_label)[0, :, 0, 0]
        
        # Do postprocess
        result_boxes = self.post_process(output, origin_h, origin_w)
        return result_boxes


    def preprocess_image(self, frame):
        """
        description:Read an image from image path, convert it to RGB,
                    resize and pad it to target size, normalize to [0,1],
                    transform to NCHW format.
        param:
            frame: np.array, image path
        return:
            image:  the processed image
            image_raw: the original image
            h: original height
            w: original width
        """
        image_raw = frame
        h, w, c = image_raw.shape
        image = cv2.cvtColor(image_raw, cv2.COLOR_BGR2RGB)
        # Calculate widht and height and paddings
        r_w = self.input_width / w
        r_h = self.input_height/ h
        if r_h > r_w:
            tw = self.input_width
            th = int(r_w * h)
            tx1 = tx2 = 0
            ty1 = int((self.input_height - th) / 2)
            ty2 = self.input_height - th - ty1
        else:
            tw = int(r_h * w)
            th = self.input_height
            tx1 = int((self.input_width- tw) / 2)
            tx2 = self.input_width - tw - tx1
            ty1 = ty2 = 0
        # Resize the image with long side while maintaining ratio
        image = cv2.resize(image, (tw, th))
        # Pad the short side with (128,128,128)
        image = cv2.copyMakeBorder(
            image, ty1, ty2, tx1, tx2, cv2.BORDER_CONSTANT, (128, 128, 128)
        )
        image = image.astype(np.float32)
        # Normalize to [0,1]
        image /= 255.0
        # HWC to CHW format:
        image = np.transpose(image, [2, 0, 1])
        # CHW to NCHW format
        image = np.expand_dims(image, axis=0)
        # Convert the image to row-major order, also known as "C order":
        image = np.ascontiguousarray(image)
        return image, h, w

    def post_process(self, output, origin_h, origin_w):
        """
        description: postprocess the prediction
        param:
            output: A tensor like [num_boxes,cx,cy,w,h,conf,cls_id, cx,cy,w,h,conf,cls_id, ...] 
        return:
            result_boxes: finally boxes, a boxes tensor, each row is a box [x1, y1, x2, y2]
            result_scores: finally scores, a tensor, each element is the score correspoing to box
            result_classid: finally classid, a tensor, each element is the classid correspoing to box
        """
        num = int(output[0])
        
        # Reshape to a two dimentional ndarray
        pred = np.reshape(output[1:], (-1, 6))[:num, :]

        """
        Process: 
            threshold - remove weak detections
            filer class boxes 
            convert to xyxy with class_id
            do nms
        """
        
        # Get the boxes
        boxes = pred[:,:6]
        scores = pred[:,4]

        # Choose those boxes that score > CONF_THRESH
        si = scores > self.conf_thresh
        boxes = boxes[si, :]
        result_classid =  boxes[:,5]
        
        ci = []
        if self.filter_class_id != None:
            for i in range(len(result_classid)):
                if int(result_classid[i]) in self.filter_class_id:
                    ci.append(True)
                else:
                    ci.append(False)
            boxes = boxes[ci, :]
        
        boxes = self.xywh2xyxy(origin_h, origin_w, boxes)
        boxes = self.nms(boxes)
        # logging.info(f'Detections: {len(boxes)}')

        return boxes

    def nms(self, boxes):
        overlapThresh = self.nms_thresh

        if len(boxes) == 0:
            return []
        
        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")
        pick = []
        x1 = boxes[:,0]
        y1 = boxes[:,1]
        x2 = boxes[:,2]
        y2 = boxes[:,3]
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = np.argsort(y2)

        while len(idxs) > 0:
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)
            overlap = (w * h) / area[idxs[:last]]
            idxs = np.delete(idxs, np.concatenate(([last],
                np.where(overlap > overlapThresh)[0])))
            
        return boxes[pick].astype("int").tolist()

    
    def xywh2xyxy(self, origin_h, origin_w, x):
        """
        description:    Convert nx4 boxes from [x, y, w, h] to 
            [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
        param:
            origin_h:   height of original image
            origin_w:   width of original image
            x:          A boxes tensor, each row is a box [center_x, center_y, w, h]
        return:
            y:          A boxes tensor, each row is a box [x1, y1, x2, y2]
        """
        y = np.zeros_like(x)
        # X, Y, X, Y, Score, CLASS_ID
        r_w = self.input_width / origin_w
        r_h = self.input_height / origin_h

        if r_h > r_w:
            y[:, 0] = x[:, 0] - x[:, 2] / 2
            y[:, 2] = x[:, 0] + x[:, 2] / 2
            y[:, 1] = x[:, 1] - x[:, 3] / 2 - (self.input_height - r_w * origin_h) / 2
            y[:, 3] = x[:, 1] + x[:, 3] / 2 - (self.input_height - r_w * origin_h) / 2
            y /= r_w
        else:
            y[:, 0] = x[:, 0] - x[:, 2] / 2 - (self.input_width - r_h * origin_w) / 2
            y[:, 2] = x[:, 0] + x[:, 2] / 2 - (self.input_width - r_h * origin_w) / 2
            y[:, 1] = x[:, 1] - x[:, 3] / 2
            y[:, 3] = x[:, 1] + x[:, 3] / 2
            y /= r_h
        
        # TO SAVE IT FROM DIVISION - APPENDING IT AFTER DIVISION        
        y[:,4] = x[:,4]*100 # scores
        y[:,5] = x[:,5]     # class_ID

        return y

    @staticmethod
    def plot_one_box(x, img, color=None, label=None):
        """
        description:Plots one bounding box on image img,
                    this function comes from YoLov5 project.
        param: 
            x:      a box likes [x1,y1,x2,y2]
            img:    a opencv image object
            color:  color to draw rectangle, such as (0,255,0)
            label:  str
            line_thickness: int
        return:
            no return
        """
        tl = round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1
        # line/font thickness
        color = (0, 0, 255)
        
        c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
        cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)

        if label:
            tf = max(tl - 1, 1)  # font thickness
            t_size = cv2.getTextSize(
                label, 0, fontScale=tl / 3, thickness=tf)[0]
            
            c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
            
            cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)#filled
            cv2.putText(
                img, label,(c1[0], c1[1] - 2),0,tl / 3,[225, 255, 255],
                thickness=tf,lineType=cv2.LINE_AA)   

if __name__ == "__main__":

    # YoLov5_TRT instance
    detector = YoloV5_Triton_Client(
        model_name = "yolov5m",
        detection_confidence=0.5,
        grpc_host="164.52.204.9",
        grpc_port=8001,
        nms_thresh = 0.6,
        filter_class_id=[0])
    # FILTER ID - LIST IDs ONLY WHAT TO DETECT

    input_path = "crowd.jpg"
    output_path = "output2.jpg"

    # Read input image    
    frame  = cv2.imread(input_path)
    h,w,c  = frame.shape
    size   = (w, h)
    
    result_boxes = detector.infer(frame)
    categories = {}
    for i in range(len(result_boxes)):
            box = result_boxes[i]
            detector.plot_one_box(
                box,frame,
                label=f"{categories[result_boxes[i][5]]} {result_boxes[i][4]}")

    success = cv2.imwrite(output_path, frame)
    print(f'Image Write Status: {bool(success)} | Written at {output_path}')