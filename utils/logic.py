import numpy as np

from woutils.postprocess import get_abs_polypoints, CentroidInPoly


def __process_poly(frame, poly_points):
    frame_h, frame_w = frame.shape[:2]
    poly_points = get_abs_polypoints(
        poly_points,
        frame_width=frame_w,
        frame_height=frame_h,
    )
    return poly_points


def get_boxes_in_poly(frame, poly_points, detections):
    poly_points = __process_poly(frame, poly_points)
    poly_object = CentroidInPoly(poly_points)
    filtered_boxes = poly_object.get_filtered_detections(detections)
    return filtered_boxes
