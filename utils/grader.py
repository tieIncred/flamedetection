__author__ = "Animikh Aich"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Animikh Aich", "Mohd Sadiq"]
__license__ = "Proprietary"
__version__ = "0.1.0"
__maintainer__ = "Animikh Aich"
__email__ = "animikh@wobot.ai"
__module_name__ = "[TicketGraderMain]"

import boto3
from cv2 import cv2
from woutils import logging
from utils.iou_scorer import IoUScore
import numpy as np


class GradingModel(IoUScore):
    __LOW = "low"
    __MEDIUM = "medium"
    __HIGH = "high"

    def __init__(
        self, region: str = None, access_id: str = None, secret_key: str = None
    ):
        # Variables
        self.grading_frame = None
        self.model_detections = None
        self.api_detections = None

        # Login to AWS
        self.aws_login(region=region, access_id=access_id, secret_key=secret_key)

    def aws_login(self, region: str, access_id: str, secret_key: str):
        """
        __init__ Class constructor to establish connection to AWS S3.
        Leave the arguments blank to automatically fetch credentials from AWS Configure

        Args:
            region (string): Enter the Region of the AWS S3 Bucket, eg. us-east-1
            access_id (string): Enter the Access ID, You can get it created from the Root AWS account
            secret_key (string): Enter the Secret Key, You can get it created from the Root AWS account
        """
        # Attempt to connect to the AWS Account
        try:
            self.client = boto3.client(
                "rekognition",
                region_name=region,
                aws_access_key_id=access_id,
                aws_secret_access_key=secret_key,
            )
            logging.info(
                f"{__module_name__} Authentication Successful, Connection established"
            )
        except Exception as e:
            logging.error(
                f"{__module_name__} Authentication Unsuccessful, Client connection failed due to Error {e}"
            )
            raise Exception(
                f"{__module_name__} Authentication Unsuccessful, Client connection failed due to Error {e}"
            )

    def aws_rekog_detect_person(self, image: np.ndarray, min_confidence: float = 0.5):
        """
        aws_rekog_detect_person Get Person Detection Coordinates using AWS Rekognition

        This uses the AWS Rekognition Protective Equipment Detection API to Send an image and get detections in that image.
        The detections contains Person, Face Cover, hand Cover, Head Cover coordinates and confidence scores.
        However, this function only focuses on Person Detection and filters the detections as per the confidence scores.

        Args:
            image (np.ndarray): Image/Frame (Raw) for Inference using AWS Rekognition
            min_confidence (float, optional): Filtering Confidence Score (For AWS Rekognition). Defaults to 0.5.

        Returns:
            list: List of Detections. Format: [xmin, ymin, xmax, ymax]
        """

        height, width = image.shape[:2]
        _, encoded_image = cv2.imencode(".jpg", image)

        response = self.client.detect_protective_equipment(
            Image={"Bytes": encoded_image.tobytes()},
            SummarizationAttributes={
                "MinConfidence": min_confidence,
                "RequiredEquipmentTypes": ["FACE_COVER", "HAND_COVER", "HEAD_COVER"],
            },
        )
        detections = []
        for person in response["Persons"]:
            bbox = person["BoundingBox"]
            xmin = bbox["Left"]
            ymin = bbox["Top"]
            xmax = xmin + bbox["Width"]
            ymax = ymin + bbox["Height"]
            detections.append(
                [
                    int(xmin * width),
                    int(ymin * height),
                    int(xmax * width),
                    int(ymax * height),
                ]
            )
        return detections

    def aws_rekog_detect_face(self, image: np.ndarray, min_confidence: float = 0.5):
        """
        aws_rekog_detect_face Get Face Detection Coordinates using AWS Rekognition

        This uses the AWS Rekognition Protective Equipment Detection API to Send an image and get detections in that image.
        The detections contains Face, Emotion, Gender, Sun Glasses, Landmarks and confidence scores.
        However, this function only focuses on Face Detection and filters the detections as per the confidence scores.

        Args:
            image (np.ndarray): Image/Frame (Raw) for Inference using AWS Rekognition
            min_confidence (float, optional): Filtering Confidence Score (For AWS Rekognition). Defaults to 0.5.

        Returns:
            list: List of Detections. Format: [[xmin, ymin, xmax, ymax]]
        """

        height, width = image.shape[:2]
        _, encoded_image = cv2.imencode(".jpg", image)

        response = self.client.detect_faces(
            Image={"Bytes": encoded_image.tobytes()},
            Attributes=["ALL"],
        )
        detections = []
        for face in response["FaceDetails"]:
            if face["Confidence"] < min_confidence * 100:
                continue

            bbox = face["BoundingBox"]
            xmin = bbox["Left"]
            ymin = bbox["Top"]
            xmax = xmin + bbox["Width"]
            ymax = ymin + bbox["Height"]
            detections.append(
                [
                    int(xmin * width),
                    int(ymin * height),
                    int(xmax * width),
                    int(ymax * height),
                ]
            )
        return detections

    def set_grading_frame(self, frame: np.ndarray):
        """
        set_grading_frame Set the Frame for Ticket Grading

        Just an easy way to input the frame that is required for Ticket Grading.
        Inference will be done on this frame. The frame can be stored and reassigned.

        Args:
            frame (np.ndarray): Image/Frame (Raw) for Inference using AWS Rekognition
        """
        self.grading_frame = frame

    def set_grading_detections(self, detections: list):
        """
        set_grading_detections Set Internal Model Detection Coordinates

        Set the Detection Coordinates that have been received from the internal model

        Args:
            detections (list): List of Detections. Format: [xmin, ymin, xmax, ymax]
        """
        self.model_detections = detections

    def grading_conditions(
        self, iou_scores: list, low_thresh: float, high_thresh: float
    ):
        """
        grading_conditions Performs the Grading Conditions

        Grading Conditions convert the list of scores to grades: low, medium, high.
        If the list of scores (passed as argument) contains 0, then a "low" grade is assigned.
        Otherwise, the mean of the list of scores is taken and is comppared with the high and low thresholds.

        Args:
            iou_scores (list): List of IoU Scores for respective Detections
            low_thresh (float): Anything below this threshold is considered "low"
            high_thresh (float): Anything above this threshold is considered "high"

        Returns:
            str: "low", "medium" or "high" - based on the converted values
        """
        # If there are any missed detections, score is always set to "LOW"
        if 0 in iou_scores:
            return self.__LOW

        # Calculate the Mean Score (Arithmetic Mean)
        score = np.mean(iou_scores)

        # Grading Conditions
        if score <= low_thresh:
            return self.__LOW
        elif low_thresh < score <= high_thresh:
            return self.__MEDIUM
        else:
            return self.__HIGH

    def grade_detections(
        self, postprocess_func: object, postprocess_config: dict, det_object="person"
    ):
        """
        grade_detections Grade the Detections using Internal Model and External API

        Internal Model Detections are pre-set by the function `set_grading_detections`. Similarly,
        the frame on which detections are to be performed is also passed and saved using `set_grading_frame`.
        This function takes the internal detections and the API Detections, passes each through the
        `postprocess_func` to filter out the unwanted detections, and then compares both to get the scores.
        The scores are then converted to Grades using `grading_conditions` method and returned.

        Args:
            postprocess_func (object): Postprocessing Function, which takes `detections` as input and returns filtered ones
            postprocess_config (dict): Config Dictionary containing `api_det_conf`, `grading_low_thresh` & `grading_high_thresh`
            det_object (string): Select whether to detect "person" or "face" or some other object

        Returns:
            str: "low", "medium" or "high" - based on the converted values
        """

        # Call External API for Detections
        if det_object == "face":
            self.api_detections = self.aws_rekog_detect_face(
                image=self.grading_frame,
                min_confidence=postprocess_config.get("api_det_conf", 0.5),
            )
        else:
            self.api_detections = self.aws_rekog_detect_person(
                image=self.grading_frame,
                min_confidence=postprocess_config.get("api_det_conf", 0.5),
            )

        # Apply postprocess to internal and external detections
        processed_model_detections = postprocess_func(self.model_detections)
        processed_api_detections = postprocess_func(self.api_detections)

        # Compare the resulting detections to get the IoU Score
        iou_scores = self.get_iou_scores(
            detections1=processed_model_detections, detections2=processed_api_detections
        )

        # Generate the Grade (High, Medium or Low)
        grade = self.grading_conditions(
            iou_scores=iou_scores,
            low_thresh=postprocess_config.get("grading_low_thresh", 0.4),
            high_thresh=postprocess_config.get("grading_high_thresh", 0.7),
        )

        logging.info(
            f"{__module_name__} Grading Stats || Grade: {grade} | Mean Score: {np.mean(iou_scores)} | IoU Scores: {iou_scores}"
        )

        return grade