__author__ = "Animikh Aich"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Animikh Aich", "Aditya Divakaran"]
__license__ = "Proprietary"
__version__ = "0.1.0"
__maintainer__ = "Animikh Aich"
__email__ = "animikh@wobot.ai"
__status__ = "Development"
__module_name__ = "[Classifier Triton Client]"

from woutils import logging
import numpy as np
import cv2

import tritonclient.grpc as grpcclient


class ClassificationClient:
    def __init__(
        self,
        model_name,
        grpc_host="localhost",
        grpc_port=8001,
        input_shape=128,
        input_label="input_1",
        output_label="activation",
        force_float_input=True,
    ):
        self.grpc_port = grpc_port
        self.grpc_host = grpc_host
        self.model_name = model_name
        self.input_label = input_label
        self.output_label = output_label
        self.input_shape = input_shape
        self.force_float_input = force_float_input

        if self.force_float_input:
            dtype = "FP32"
        else:
            dtype = "UINT8"

        self.inputs = []
        self.outputs = []
        self.inputs.append(
            grpcclient.InferInput(
                self.input_label, [-1, input_shape, input_shape, 3], dtype
            )
        )

        self.outputs.append(grpcclient.InferRequestedOutput(self.output_label))

        try:
            self.triton_client = grpcclient.InferenceServerClient(
                url=f"{self.grpc_host}:{self.grpc_port}"
            )

        except Exception as e:
            logging.error(f"{__module_name__} Channel Creation Failed: {e}")
            raise Exception(f"{__module_name__} Channel Creation Failed: {e}")


    def __preprocess_image(self, frame, force_float=False, channel_format="bgr"):
        if channel_format == "bgr":
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if force_float:
            frame = frame.astype(np.float32)
        frame = cv2.resize(frame, (self.input_shape, self.input_shape))
        frame *= (1.0 / 255.0)  # normalise
        frame = np.expand_dims(frame, axis=0)
        return frame

    def __postprocess_output(self, preds):
        return np.argmax(np.squeeze(preds))

    def classify(
        self, frame, channel_format="bgr",
    ):
        input_frame = frame.copy()

        input_frame = self.__preprocess_image(
            frame=input_frame,
            force_float=self.force_float_input,
            channel_format=channel_format,
        )
        self.inputs[0].set_data_from_numpy(input_frame)

        # Setting gRPC channel and Sending request for detection
        result = self.triton_client.infer(
            model_name=self.model_name,
            inputs=self.inputs,
            outputs=self.outputs,
            headers={"test": "1"},
        )

        output = np.squeeze(result.as_numpy(self.output_label))
        output = self.__postprocess_output(output)

        return output

    def __get_cropped_frame(self, frame, detection):
        xmin, ymin, xmax, ymax = detection
        return frame[ymin:ymax, xmin:xmax]

    def classify_detections(self, frame, detections, channel_format="bgr"):
        classes = []
        for detection in detections:
            cropped_frame = self.__get_cropped_frame(frame, detection)
            classes.append(self.classify(cropped_frame, channel_format=channel_format))

        return detections, classes

