__author__ = "Animikh Aich"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Animikh Aich"]
__license__ = "Proprietary"
__version__ = "0.2.0"
__maintainer__ = "Animikh Aich"
__email__ = "animikh@wobot.ai"
__module_name__ = "[Ticket Grading]"

import numpy as np
import logging


class IoU:
    """
    Calculate IoU Score for Two Sets of Detections
    IoU = Intersection over Union
    IoU = Area of Intersection / Area of Union of Two Boxes
    """

    def __calc_intersection(self, box1, box2):
        """
        __calc_intersection Calculate the Intersection of Two Bounding Boxes

        Intersection is calculated by calculating the overalpping area of two bounding boxes.
        If the boxes do not overlap, then intersection value is 0
        If the boxes do overlap, then the intersection value is the area of overlap with unit pixels

        Args:
            box1 (list): Coordinates of a Bounding Box 1, format: `[xmin, ymin, xmax, ymax]`
            box2 (list): Coordinates of a Bounding Box 2, format: `[xmin, ymin, xmax, ymax]`

        Returns:
            int: Intersection Area of the two Bounding Boxes (in Sq. Pixels)
        """
        assert any(
            self.__bbox_validity_check(box1, box2)
        ), "Invalid BBox Coordinates Received"

        # Unwrap the coordinates
        xmin1, ymin1, xmax1, ymax1 = box1
        xmin2, ymin2, xmax2, ymax2 = box2

        # Calculate the inner box created by the overlap of the two boxes
        xmin = max(xmin1, xmin2)
        xmax = min(xmax1, xmax2)
        ymin = max(ymin1, ymin2)
        ymax = min(ymax1, ymax2)

        # Calculate the width and height of the inner box (for Area Calculation)
        width = xmax - xmin
        height = ymax - ymin

        # Calculate the overlapping area (in sq. pixes)
        # If there is no overlap then the area is set to 0
        if width <= 0 or height <= 0:
            intersection = 0
        else:
            intersection = width * height
        return intersection

    def __bbox_validity_check(self, *boxes):
        """
        __bbox_validity_check Validity Check for the Bounding Box

        Takes in one or more bounding boxes
        Checks:
        - if xmin is less than xmax
        - if ymin is less than ymax

        If Either of these two are not satisfied, it appends `False` to the result, else appends `True`

        Returns:
            list: List of Booleans, as per the checks for each box passed
        """
        result = []

        for box in boxes:
            xmin, ymin, xmax, ymax = box
            if xmin < xmax and ymin < ymax:
                result.append(True)
            else:
                result.append(False)

        return result

    def __calc_union(self, box1, box2):
        """
        __calc_union Calculate the Union of the Two boxes in Question

        Calculates the Area of the Union of the Two bounding boxes (in sq. pixels)
        Union = Sum of Areas of the Two Independent Bounding Boxes, with the Intersection Subtracted from it
        Union = Sum(Area(box1), Area(box2)) - Intersection(box1, box2)

        Args:
            box1 (list): Coordinates of a Bounding Box 1, format: `[xmin, ymin, xmax, ymax]`
            box2 (list): Coordinates of a Bounding Box 2, format: `[xmin, ymin, xmax, ymax]`

        Returns:
            int: Union Area of the two Bounding Boxes (in Sq. Pixels)
        """

        assert any(
            self.__bbox_validity_check(box1, box2)
        ), "Invalid BBox Coordinates Received"

        xmin1, ymin1, xmax1, ymax1 = box1
        xmin2, ymin2, xmax2, ymax2 = box2

        area1 = (xmax1 - xmin1) * (ymax1 - ymin1)
        area2 = (xmax2 - xmin2) * (ymax2 - ymin2)

        intersection = self.__calc_intersection(box1, box2)

        union = area1 + area2 - intersection
        return union

    def calc_iou(self, box1, box2):
        """
        calc_iou Calculate the Intersection over Union

        IoU = Intersection(box1, box2) / Union(box1, box2)

        Score Range:
        - Score 0: When there is no overlap between the two boxes
        - Score 1: When there is 100% overlap between the two boxes (They are exactly the same)
        - For any other case, the score will lie between 0 and 1

        Args:
            box1 (list): Coordinates of a Bounding Box 1, format: `[xmin, ymin, xmax, ymax]`
            box2 (list): Coordinates of a Bounding Box 2, format: `[xmin, ymin, xmax, ymax]`

        Returns:
            float: IoU Score, Range: 0 ot 1, Inclusive
        """
        # IoU = Intersection over Union of BBoxes
        try:
            intersection = self.__calc_intersection(box1, box2)
            union = self.__calc_union(box1, box2)
        except AssertionError as e:
            logging.error(
                f" {__module_name__} Assertion Error Encountered: {box1, box2}, Error: {e}"
            )
            iou = 0

        try:
            iou = intersection / union
        except ZeroDivisionError as e:
            logging.error(
                f"{__module_name__} Zero Division Error for IoU Calculation for Boxes: {box1, box2}, Error: {e}"
            )
            iou = 0

        return iou


class IoUScore(IoU):
    """
    GradeTickets Class to Grade the Tickets

    Ticket Grading Class, Inherits from IoU Class.
    - It takes in the predictions of two detectors, and gives back the IoU scores
    - It also can take in two sets of centroids and return batches of centroids that are closest to each other

    Inheritance:
        IoU (class): Calculate IoU Score for Two Sets of Detections
    """

    def __calc_euclidean_dist(self, x1, y1, x2, y2):
        """
        __calc_euclidean_dist

        Calculate the Euclidean Distance Between two points

        Args:
            x1 (int): xmin
            y1 (int): ymin
            x2 (int): xmax
            y2 (int): ymax

        Returns:
            float: Euclidean Distance Between the Two points (x1, y1) and (x2, y2)
        """
        return np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    def get_closest_centroid_pairs(self, centroids1, centroids2):
        """
        get_closest_centroid_pairs

        Given two sets of Lists containing Centroids (x, y) coordinates,
        This calculates the corresponding pairs from each list which are closest to each other,
        and returns a List of Tuplies, containing pairs of the closest centroids


        Args:
            centroids1 (list/tuple): First list of Centroids from Model 1 in the form (x, y)
            centroids2 (list/tuple): Second list of Centroids from Model 2 in the form (x, y)

        Returns:
            list: Pairs of Centoids Coupled together as per the closest between two lists
        """
        # TODO: Optimize
        # centroids1 => Centroids of Predictions of Model 1
        # centroids2 => Centroids of Predictions of Model 2
        # i => i-th index of result array - Model 1 Distances
        # j => j-th index of result array - Model 2 Distances

        # Create an Matrix with default values
        distances = np.ones((len(centroids1), len(centroids2)), dtype=np.float32) * 9999

        # Calculate the Distances for each pair of centroids from the two frames
        for i, (xi, yi) in enumerate(centroids1):
            for j, (xj, yj) in enumerate(centroids2):
                distances[i, j] = self.__calc_euclidean_dist(xi, yi, xj, yj)

        # Create a list of centroid paris which are closest to each other
        centroid_pairs = list()

        # A "Hacky" way to select the axis, works for now, but needs to be looked at
        if len(centroids1) >= len(centroids2):
            axis = 1
        else:
            axis = 0

        for i, min_dist_index in enumerate(np.argmin(distances, axis=axis)):
            # A "Hacky" way to find the minimum distance, works for now, but needs to be looked at
            if axis == 1:
                centroid_pairs.append((centroids1[i], centroids2[min_dist_index]))
            else:
                centroid_pairs.append((centroids1[min_dist_index], centroids2[i]))

        return centroid_pairs

    def get_centroid(self, box):
        """
        get_centroid

        Calculate Centroid of a BBox and return the same

        Args:
            box (list): Bounding Box in the format: [xmin, ymin, xmax, ymax]

        Returns:
            tuple: Centroid of the BBox (cx, cy)
        """

        xmin, ymin, xmax, ymax = box
        cx = (xmin + xmax) // 2
        cy = (ymin + ymax) // 2
        return cx, cy

    def get_centroid_box_pair(self, boxes):
        """
        get_centroid_box_pair

        Get the Respective Centroid and Box Pairs in the form of a dictionary
        This is used to calculate and add centroids as `keys` and the box as `values`
        Return format: `{(cx, cy): [xmin, ymin, xmax, ymax]}`

        Args:
            boxes (list): List of Bounding Boxes, format: [[xmin, ymin, xmax, ymax],]

        Returns:
            dict: Centroid and its corresponding box pair. Format: `{(cx, cy): [xmin, ymin, xmax, ymax]}`
        """
        cent_box = dict()

        for box in boxes:
            centroid = self.get_centroid(box)
            cent_box[centroid] = box

        return cent_box

    def get_iou_scores(self, detections1, detections2):
        """
        get_iou_scores Get the IoU Scores

        Get the IoU scores for each pair of Detections.
        Detections 1 and 2 are compared and closest boxes across both are identified.
        IoU scores are calculated for each of the closest pairs and then returned as list

        Args:
            detections1 (list): List of Coordinates of Model 1 Preds, format: `[[xmin, ymin, xmax, ymax]]`
            detections2 (list): List of Coordinates of Model 2 Preds, format: `[[xmin, ymin, xmax, ymax]]`

        Returns:
            list: IoU scores calculated as per the closest pairs
        """

        # If both models have no detections, then return 1 (Object Absent)
        if len(detections1) == len(detections2) == 0:
            return [1.0]
        # Else if one of the models have no detections, then return 0 (Missed Detections)
        elif len(detections1) == 0 or len(detections2) == 0:
            return [0.0]

        # Get the Centroid Box Pairs: Key = Centroid (tuple), Value = Boxes (list)
        cent_box1 = self.get_centroid_box_pair(detections1)
        cent_box2 = self.get_centroid_box_pair(detections2)

        # Get List of Keys
        centroids1 = list(cent_box1.keys())
        centroids2 = list(cent_box2.keys())

        # Get the Pairs of Centroids which are closest to one another
        centroid_pairs = self.get_closest_centroid_pairs(centroids1, centroids2)

        # Calculate the corresponding IoU Scores
        iou_scores = list()
        for centroid_pair in centroid_pairs:
            c1, c2 = centroid_pair
            box1 = cent_box1[c1]
            box2 = cent_box2[c2]
            iou_scores.append(self.calc_iou(box1, box2))

        return iou_scores


if __name__ == "__main__":
    boxes1 = [
        [898, 157, 967, 227],
        [413, 261, 494, 335],
        [649, 287, 728, 370],
        [705, 162, 763, 222],
        [614, 318, 695, 392],
    ]

    boxes2 = [[614, 318, 695, 392], [428, 279, 509, 353], [688, 150, 782, 236]]

    scorer = IoUScore()
    print(scorer.get_iou_scores(boxes1, boxes2))
