__author__ = "Animikh Aich"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Animikh Aich", "Karan Acharya"]
__license__ = "Proprietary"
__version__ = "0.2.0"
__maintainer__ = "Animikh Aich"
__email__ = "animikh@wobot.ai"
__status__ = "Production"
__module_name__ = "[Ticketing]"


from datetime import date, datetime
import os
from threading import Thread

from cv2 import cv2
import requests

from woutils import valid_frame_checks, valid_file_checks, logging
from woutils.aws import S3Utils
from woutils.security import PayloadEncryption

import imutils

# TODO: Add a separate function to remove special characters
# TODO: Split Large Functions into smaller ones


class Ticketing(S3Utils, PayloadEncryption):
    """
    Ticketing Ticketing Class

    Inherits from S3Utils Class, to implement AWS S3 Uploading as well

    Features:
    - Automatically Uploads Image and Video and Raises the Ticket
    - Automatically Determines the Specified Ticket Title (Can be overridden)
    - Video clips that are passed, are first converted to playable format and then uploaded
    - If an image is passed, automatically checks if it is a file or a image array & uploads it
    - Validation Checks are in place for both images and videos
    - Even if Videos are somehow fails, tickets are still raised with the image
    - Ticket raising will fail without image. Valid Image is mandatory to raise ticket

    Args:
        S3Utils (class): Inherits from S3 Utility to Upload and Download files to/from AWS S3
    """

    TIMESTAMP_STR_FORMAT = "%H_%M_%S__%d_%m_%Y"
    TIME_STR_FORMAT = "%H_%M_%S"
    DATE_STR_FORMAT = "%d_%m_%Y"
    IMAGE_METADATA = {"ContentType": "image/jpeg"}
    VIDEO_METADATA = {"ContentType": "video/mp4"}
    IMAGE = "img"
    FILE = "file"
    UNK = "unknown"

    def __init__(
        self,
        ticketing_endpoint: str = "https://api-app-prod.wobot.ai/public/v1/event/create",
        task: str = "603ce400bf7ada6be0dd55cc",
        usecase: str = "Test_Case",
        dirpath: str = "Test_Directory_Path",
        aws_bucket_name: str = "test-wobot-delhi",
        aws_access_id: str = None,
        aws_secret_key: str = None,
        aws_region: str = "ap-south-1",
    ):
        """
        __init__ Initialize Default Values & AWS S3 Login

        Set the Parameters that govern the ticket raising and AWS uploading process

        Args:
            ticketing_endpoint (str, optional): Ticketing URL Endpoint. Defaults to "http://api-app-prod.wobot.ai/public/v1/event/create".
            task (str, optional): Hash String that identifies the Task. Defaults to Test Hash String.
            usecase (str, optional): Ticketing Sub Issue, Eg. Face Mask Violation. Defaults to "Test_Case".
            dirpath (str, optional): Directory Path in Bucket. Defaults to "Test_Directory_Path".
            aws_bucket_name (str, optional): AWS S3 Bucket Name. Defaults to "test-wobot-delhi".
            aws_access_id (str, optional): AWS Access ID, Credentials. Defaults to None.
            aws_secret_key (str, optional): AWS Secret Key, Credentials. Defaults to None.
            aws_region (str, optional): AWS Region, Default is Mumbai, India. Defaults to "ap-south-1".
        """
        PayloadEncryption.__init__(self)
        S3Utils.__init__(
            self, region=aws_region, access_id=aws_access_id, secret_key=aws_secret_key
        )
        self.ticketing_endpoint = ticketing_endpoint

        # Initializing a few Default Ticketing Parameters
        self.bucket_name = aws_bucket_name
        self.company = "5f718a3229d5f02e2e2affd7"
        self.location = "6010f9cb2c722321166fb24b"
        self.camera = "60068fb9fc357b071b81139d"
        self.schedule = "601d09e30577d057ae4458ea"
        self.task = task
        self.usecase = usecase
        self.dirpath = dirpath

    def __raise_ticket(
        self,
        image_url: str,
        video_url: str = "",
        confidence: str = "",
        e_id: list = [],
        is_ticket: bool = True,
    ):
        """
        __raise_ticket Raise Tickets given Image/Video URL

        Args:
            image_url (str): AWS S3 File URL For Image
            video_url (str, optional): AWS S3 File URL For Video. Defaults to "".
            confidence (str, optional): Confidence in Ticket - `high`, `medium`, `low`. Defaults to "".
            e_id (list, optional): List of Strings, for FR-based Employee Name/ID. Defaults to [].
            is_ticket (bool, optional): Raise it as a ticket or as an event.

        Returns:
            (bool, str): True if Success, else False with Response Message
        """

        # Some Fault Tolerant Checks
        if image_url is None:
            image_url = ""
        if video_url is None:
            video_url = ""

        # Payload and Headers
        payload = {
            "video": video_url,
            "image": image_url,
            "confidence": confidence,
            "e_id": e_id,
            "company": self.company,
            "camera": self.camera,
            "location": self.location,
            "task": self.task,
            "schedule": self.schedule,
            "isTicket": is_ticket,
        }
        headers = {
            "Content-Type": "application/json",
            "auth-token": self.encrypt_payload(payload),
        }
        # Send the REST Request to raise a ticket
        try:
            response = requests.request(
                "POST",
                self.ticketing_endpoint,
                data={},
                headers=headers,
                timeout=10,
            )
            logging.info(
                f"{__module_name__} Ticket Raised with payload: {payload} at URL: {self.ticketing_endpoint} with Response: {response.text}"
            )
            logging.info(
                f"{__module_name__} Encrypted Ticket Payload: {self.encrypt_payload(payload)}"
            )
        except Exception as e:
            logging.critical(f"{__module_name__} Failed to Raise Ticket. Error: {e}")
            return False, f"Error: {e}"

        return True, response.text

    def __check_image_or_file(self, image):
        """
        __check_image_or_file

        Check if the passed argument is an image, or file, or none of them

        Args:
            image (str/numpy array): Can be a Path or Numpy Array

        Returns:
            (bool, str): True if known format, else False with type of image
        """
        if isinstance(image, str) and valid_file_checks(image):
            return True, self.FILE
        elif valid_frame_checks(image):
            return True, self.IMAGE
        else:
            return False, self.UNK

    def __gen_generic_filename(self):
        """
        __gen_generic_filename

        Generate Filename in a given format

        Returns:
            str: Filename
        """
        timestamp_string = datetime.now().strftime(self.TIMESTAMP_STR_FORMAT)
        filename = f"{self.usecase}_{timestamp_string}".replace(" ", "_")

        return filename

    def generate_ticket(
        self,
        company: str,
        location: str,
        camera: str,
        schedule: str,
        image: str,
        video: str = None,
        confidence: str = None,
        e_id: list = None,
        is_ticket: bool = True,
        num_video_conversion_threads: int = 1,
    ):
        """
        generate_ticket Upload Image/Video and Raise Ticket

        Do Validity Checks, and Upload the Image/File, followed by Raise Ticket

        Features:
        - Auto Checks and Handles if an NumPy image or a image file is passed
        - If a NumPy Image is passed, it writes the frame and then uploads
        - Auto delete the file(s) once upload is complete
        - Video Input is optional, but Image is mandatory
        - Videos are automatically converted and then uploaded in a new thread
        - Ticketing Title is Auto Generated internally, but if required, custom title can be passed

        Args:
            company (str): Name of the Company as a Hash String
            location (str): Name of the Location as a Hash String
            camera(str): Name of the Camera as a Hash String
            schedule(str): Schedule of the Use Case as a Hash String
            image (str): Image Frame, or Image File Path (Both works)
            video (str, optional): Video File Path. Defaults to None.
            confidence (str, optional): Confidence in Ticket - `high`, `medium`, `low`. Defaults to "".
            e_id (list, optional): List of Strings, for FR-based Employee Name/ID. Defaults to [].
            is_ticket (bool, optional): Raise it as a ticket or as an event.
            num_video_conversion_threads (int, optional): Number of threads the video converter will use. Defaults to 1.

        Returns:
            [type]: [description]
        """
        # Initialize the constants
        video_url = None
        image_url = None
        self.company = company
        self.location = location
        self.camera = camera
        self.schedule = schedule

        # Verify and Upload Video
        if video is not None and not valid_file_checks(video):
            video = None
        if video is not None:
            try:
                # Get Existing File Extension
                extension = os.path.splitext(video)[-1]
                if len(extension) < 3:
                    extension = ".mp4"

                # Create New Video Filename & Rename
                video_filename = self.__gen_generic_filename() + extension
                os.rename(video, video_filename)

                directory_path = f"{self.dirpath}/clips"

                # Convert & Upload Video (on a new thread)
                logging.info(
                    f"{__module_name__} Initializing Video Upload on a new thread"
                )

                upload_video_thread = Thread(
                    target=self.convert_and_upload,
                    args=(
                        self.bucket_name,
                        video_filename,
                        num_video_conversion_threads,
                        True,
                        directory_path,
                        self.VIDEO_METADATA,
                    ),
                )

                upload_video_thread.start()

                video_url = f"{directory_path}/{video_filename}"
            except Exception as e:
                logging.error(
                    f"{__module_name__} Video Upload Failed, Raising ticket without video. Error: {e}"
                )

        """
        # Verify and Upload Image (Needs to be converted into functions)
        success, image_type = self.__check_image_or_file(image)

        if image_type == self.FILE:
            try:
                # Get Existing File Extension
                extension = os.path.splitext(image)[-1]
                if len(extension) < 3:
                    extension = ".jpg"

                # Create New image Filename & Rename
                image_filename = self.__gen_generic_filename() + extension
                os.rename(image, image_filename)

                directory_path = f"{self.dirpath}/snaps"

                logging.info(
                    f"{__module_name__} Initializing Image Upload (on the main thread)"
                )
                self.upload(
                    self.bucket_name,
                    image_filename,
                    True,
                    directory_path,
                    self.IMAGE_METADATA,
                )
                image_url = f"{directory_path}/{image_filename}"
            except Exception as e:
                logging.error(
                    f"{__module_name__} Image Upload Failed, Ticket Will Not be raised. Error: {e}"
                )
        elif image_type == self.IMAGE:
            try:
                # Create Filename, Write the image, and Upload it.
                image_filename = self.__gen_generic_filename() + ".jpg"
                cv2.imwrite(image_filename, image)

                directory_path = f"{self.dirpath}/snaps"

                logging.info(
                    f"{__module_name__} Initializing Image Upload (on the main thread)"
                )
                self.upload(
                    self.bucket_name,
                    image_filename,
                    True,
                    directory_path,
                    self.IMAGE_METADATA,
                )
                image_url = f"{directory_path}/{image_filename}"
            except Exception as e:
                logging.error(
                    f"{__module_name__} Image Upload Failed, Ticket Will Not be raised. Error: {e}"
                )
        else:
            return False, "Error: Invalid Image Passed, Ticket Not Raised."
        """

        ######################################## MODIFIED ########################################

        # Verify and Upload Image (Needs to be converted into functions)
        success, image_type = self.__check_image_or_file(image)

        if image_type == self.FILE:
            try:
                image = cv2.imread(image)
                image = imutils.resize(image, height=600)

            except Exception as e:
                logging.error(f"{__module_name__} Image resize failed. Error: {e}")
        elif image_type == self.IMAGE:
            try:
                image = imutils.resize(image, height=600)
            except Exception as e:
                logging.error(f"{__module_name__} Image resize failed.. Error: {e}")
        else:
            return False, "Error: Invalid Image Passed, Ticket Not Raised."

        try:
            # Create Filename, Write the image, and Upload it.
            image_filename = self.__gen_generic_filename() + ".jpg"
            cv2.imwrite(image_filename, image)

            directory_path = f"{self.dirpath}/snaps"

            logging.info(
                f"{__module_name__} Initializing Image Upload (on the main thread)"
            )
            self.upload(
                self.bucket_name,
                image_filename,
                True,
                directory_path,
                self.IMAGE_METADATA,
            )
            image_url = f"{directory_path}/{image_filename}"
        except Exception as e:
            logging.error(
                f"{__module_name__} Image Upload Failed, Ticket Will Not be raised. Error: {e}"
            )

        ######################################## xx-MODIFIED-xx ########################################

        # Raise Ticket
        success, message = self.__raise_ticket(
            image_url=image_url,
            video_url=video_url,
            confidence=confidence,
            e_id=e_id,
            is_ticket=is_ticket,
        )

        return success, message

    def generate_test_ticket(self, image_path: str, video_path: str = None):
        """
        generate_test_ticket Generate test ticket

        Get the Image path and (optional) Video path and pass it to generate_ticket() method

        Args:
            image_path (str): Image File Path
            video_path (str, optional): Video File Path. Defaults to None.

        Returns:
            -
        """

        success, message = self.generate_ticket(
            company=self.company,
            location=self.location,
            camera=self.camera,
            schedule=self.schedule,
            image=image_path,
            video=video_path,
        )

        print(image_path)

        if success:
            logging.info(
                f"{__module_name__} Test ticket generation successful. {message}"
            )
        else:
            logging.error(
                f"{__module_name__} Failed to generate test ticket. {message}"
            )


if __name__ == "__main__":
    ticketing = Ticketing(aws_access_id="access_id", aws_secret_key="secret_key")
    ticketing.generate_test_ticket("/path/to/image.jpg")
