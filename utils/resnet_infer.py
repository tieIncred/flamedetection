__author__ = "Aadil Srivastava"
__copyright__ = "Copyright 2021, Wobot Intelligence Pvt. Ltd."
__credits__ = ["Aadil Srivastava"]
__license__ = "Proprietary"
__version__ = "1.0.0"
__maintainer__ = "Aadil Srivastava"
__email__ = "aadil.srivastava@wobot.ai"
__status__ = "Development"
__module_name__ = "[ResNet Triton Client]"

import numpy as np
import tritonclient.grpc as grpcclient

from PIL import Image
from woutils import logging

class ResNet_Triton_Client:

    def __init__(
        self,
        model_name="resnet",
        grpc_host='localhost',
        grpc_port=8001,
        input_layer='input_1',
        output_layer='activation',
        input_shape=128

    ) -> None:
        
        self.grpc_port      = grpc_port
        self.grpc_host      = grpc_host
        self.model_name     = model_name
        self.input_width    = input_shape
        self.input_height   = input_shape
        # self.conf_thresh    = detection_confidence
        self.input_layer_name = input_layer
        self.output_layer_name = output_layer

        self.inputs  = []
        self.outputs = []
        self.labels = ['Bill', 'No_Bill']

        self.inputs.append(grpcclient.InferInput(
            self.input_layer_name, 
            [1, 3, self.input_height,
            self.input_width], "FP32"))

        self.outputs.append(
            grpcclient.InferRequestedOutput(
                self.output_layer_name))

        try:
            self.triton_client = grpcclient.InferenceServerClient(
                url=f"{self.grpc_host}:{self.grpc_port}")

        except Exception as e:
            logging.error(
                f"{__module_name__} Channel Creation Failed: {e}")
            raise Exception(
                f"{__module_name__} Channel Creation Failed: {e}")

    
    def infer(self, input_image_path) -> str:
        # Preprocessing Image
        input_image = image_transform_onnx(
            input_image_path, 
            self.input_height
        )
        # Initialize the data
        self.inputs[0].set_data_from_numpy(input_image)
        # Calling Model on Triton Server
        results = self.triton_client.infer(
            model_name=self.model_name,
            inputs=self.inputs,
            outputs=self.outputs,
            headers={}
        )

        output = np.squeeze(results.as_numpy(self.output_layer_name))
        return self.labels[np.argmax(output)]

def image_transform_onnx(img, size: int) -> np.ndarray:
    '''Image transform helper for onnx runtime inference.'''

    image = Image.fromarray(img)
    image = image.resize((size,size))

    # now our image is represented by 3 layers - Red, Green, Blue
    # each layer has a size x size values representing
    image = np.array(image)

    # dummy input for the model at export - torch.randn(1, 3, 224, 224)
    image = image.transpose(2,0,1).astype(np.float32)

    # our image is currently represented by values ranging between 0-255
    # we need to convert these values to 0.0-1.0 - those are the values that are expected by our model

    image /= 255

    # expanding the alread existing tensor with the final dimension (similar to unsqueeze(0))
    # currently our tensor only has rank of 3 which needs to be expanded to 4 - torch.randn(1, 3, 224, 224)
    # 1 can be considered the batch size

    image = image[None, ...]
    return image