# Flame Detection in Absence of Person #
A NVIDIA Triton Inference Server-based program to detect flame and person attenting the flame at a kitchen,
and consequently raise tickets if a flame was detected with no person present.